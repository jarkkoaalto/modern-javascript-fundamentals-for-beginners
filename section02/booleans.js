let okay = true;
let notOkay = false;
let message = "Hello JavaScript";

let checker = message.includes('JavaScript');

console.log(notOkay);
console.log(okay);
console.log(message);
console.log(checker);

console.log(5==5);
console.log(5!=5);
console.log(10==5);
console.log('5'==5); // True
console.log('5'===5); // false
console.log('5' !== 5); // true

console.log(typeof('5')); // String
console.log(typeof(5)); // number 
console.log(typeof(true)); // boolean