const message = function(){
    console.log("Hello World expression");
}

message();

function message2(mes='default') {
    console.log("Hello world function declaration " + mes);
    return mes;
}

message2();

let str = message2("hi there");