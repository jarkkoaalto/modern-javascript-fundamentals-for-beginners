let greeting = "Hello world";
let js = 'JavaScript';
let mes1 = "i\'d rather be coding " + js;
let mes2 = `i'd rather be coding ${js}`;
console.log(mes1);
console.log(mes2);

let mes3 = `
<h1>${js}</h1>
<p>${mes1}</p>
<p>${mes2}</p>
`;
console.log(mes3);