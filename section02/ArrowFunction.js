const addme = function(a=0,b=0) {
    console.log(a,b);
    return a + b;
}

console.log(addme());
console.log(addme(5));
console.log(addme(6,7));

const adder =(a=0,b=0) => {
    console.log(a,b);
    return a + b + 10;
}

console.log(adder());
console.log(adder(5));
console.log(adder(8,9));

const adder2 = (a=0,b=0) => a + b +10;

console.log(adder2());
console.log(adder2(3));
console.log(adder2(6,7));