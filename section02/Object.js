let car = {
    make: 'ford',
    model: 'mustang',
    year: 2000,
    price: 50000,
    color: 'red',
    tires: true,
    drive() {
        console.log('its driving');
    }
    , instructions: ['Turn key', 'Put in gear','press gas pedal']
    ,help(){
        console.log(this);
    }
    ,info(){
        console.log(`Make ${this.make} Model ${this.model}`);
    }
    ,howto(){
        this.instructions.forEach(step => {
            console.log(step);
        })
    }
};

console.log(car);
car.color = 'blue';
console.log(car);
car['color'] = 'green';
console.log(car);

let prop = 'color';
car[prop] = 'black';
console.log(car);


car.inside = ['radio','seats','steering wheel','true'];
car.outside = {
    tires: 4,
    windshield: 'glass',
    bumper: true
}

console.log(car.outside);
console.log(car.inside);


