let friends = {
    firstName : 'John',
    lastName : 'Smith',
    age: 40
};
console.log(Object.keys(friends));
console.log(Object.values(friends));

for(prop in friends){
    console.log(prop);
    console.log(friends[prop]);
}

let friends2 = {
    firstName : 'Jane',
    lastName : 'Doe',
    age: 25
};
for(prop in friends2){
    console.log(prop);
    console.log(friends2[prop]);
}

let frieds = {
    list: [friends, friends2]
};
console.log(friends.list);

friends.list.forEach(function(friends){
    console.log(`${friends.firstName} ${friends.lastName}`);
    
});