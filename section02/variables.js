
// Declare variables using let

let message="hello world"
console.log(message);

message ="New Value";
console.log(message);

let val = 'string variable';
console.log(val);
val = "updated";
console.log(val);
console.log(typeof(val));
let val1 = 1;
console.log(typeof(val1));

// Declare variables using const

const val3 = "Hello World";
console.log(typeof(val3))