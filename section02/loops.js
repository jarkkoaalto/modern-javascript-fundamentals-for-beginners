for(let x = 0; x<5;x++) {
    console.log(x);
}

console.log('Loop completed');
let y = 0;

while(y<5){
    console.log(y);
    y++;
}

console.log('while loop completed');
let z = 0;

do{
    console.log(z);
    z++;
}

while(z<5);
console.log("dowhile loop completed");


let friends =['Mike', 'John','Lisa','Jane'];
for(let i=0;i<friends.length;i++)
{
    console.log(friends[i]);
}

let j =0;
while(j < friends.length){
    console.log(j);
    j++;

}