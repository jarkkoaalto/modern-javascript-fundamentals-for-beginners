let friends = ['Mike','John','Lisa','Jane'];
console.log(friends.length);
console.log(friends);

friends[3] = 'Lee';
console.log(friends);

let mixer = ['hello',50,true, 100,600,null,[1,2,3]];

console.log(mixer);
mixer[6][1] = 500;
console.log(mixer);
console.log(mixer.toString());

// Join strings

let joiner = friends.join('---*---');
console.log(joiner);


let itemLast = friends.pop();
console.log(itemLast);

friends.push("Linda");
console.log(friends);

let itemFirst = friends.shift();
friends.unshift('Bob');
let middle = friends.splice(2);
console.log(friends);

// adding
let first = friends.splice(0,1);
console.log(first);

// new array
let friends2 = ['Mike','John','Lisa','Jane'];
let myArray = friends.concat(friends2);
console.log(myArray)